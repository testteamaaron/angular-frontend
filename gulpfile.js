var gulp = require('gulp');
var concat = require('gulp-concat');
var webserver = require('gulp-webserver');
// var imagemin = require('gulp-imagemin');
var sourcemaps = require('gulp-sourcemaps');
var del = require('del');

var paths = {
  scripts: ['app/pages/**/*.js', 'app/services/**/*.js', 'app/components/**/*.js',
            '!app/bower_components/**/*.js', '!app/app.js', '!app/dist/*'],
  images: 'client/img/**/*'
};

// Not all tasks need to use streams
// A gulpfile is just another node program and you can use any package available on npm
gulp.task('clean', function() {
  // You can use multiple globbing patterns as you would with `gulp.src`
  return del(['build']);
});

gulp.task('scripts', ['clean'], function() {
  // Minify and copy all JavaScript (except vendor scripts)
  // with sourcemaps all the way down
  return gulp.src(paths.scripts)
    .pipe(sourcemaps.init())
      .pipe(concat('all.js'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('app/dist/'));
});

// Copy all static images
// gulp.task('images', ['clean'], function() {
//   return gulp.src(paths.images)
//     // Pass in options to the task
//     .pipe(imagemin({optimizationLevel: 5}))
//     .pipe(gulp.dest('build/img'));
// });

// Rerun the task when a file changes
gulp.task('watch', function() {
  gulp.watch(paths.scripts, ['scripts']);
  gulp.watch(paths.images, ['images']);
});

gulp.task('webserver', function() {
  gulp.src('app')
    .pipe(webserver({
      livereload: false,
      open: true
    }));
});

// The default task (called when you run `gulp` from cli)
gulp.task('default', ['watch', 'scripts', 'webserver']);
