'use strict';

angular.module('swiss.start', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/start', {
    templateUrl: 'pages/start/start.html',
    controller: 'startCtrl',
    controllerAs: 'vm'
  });
}])

.controller('startCtrl',
            ["TournamentManager",
             "$location", function(TournamentManager, $location) {
    var vm = this;   
    vm.resolveTournament = function(preTourn) {
        
    }    

    vm.newTournament = function(preTourn) {
        TournamentManager.newTourn(preTourn).success(function(data) {
            vm.tourn = data;
            $location.path(vm.tourn.uuid + "/add-players/");
        });
    }
   
}]);
