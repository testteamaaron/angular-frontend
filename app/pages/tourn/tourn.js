'use strict';

angular.module('swiss.tourn', ['ngRoute', 'ngAnimate', 'ui.bootstrap'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/:uuid/tourn', {
    templateUrl: 'pages/tourn/tourn.html',
    controller: 'tournCtrl',
    controllerAs: 'vm'
  });
}])

.controller('tournCtrl',
            ["TournamentManager", "TournamentRunner", "$location", "$routeParams",  "$uibModal",
             function(TournamentManager, TournamentRunner, $location, $routeParams, $uibModal) {
    var vm = this;   
    
    vm.uuid = $routeParams.uuid; 
    TournamentManager.fetchTourn(vm.uuid) 
         .success(function(data) {
            vm.tourn = data;
       }).error(function(data) {
            $location.path("/");
    });

  vm.finalise = function() {
    TournamentRunner.completeRound(vm.uuid)
      .success(function(data) {
        vm.tourn = data;
      });
  };

  vm.open = function (pairing) {
    var modalInstance = $uibModal.open({
      animation: true,
      templateUrl: 'myModalContent.html',
      controller: 'ModalInstanceCtrl',
      controllerAs: 'mic',
      resolve: {
        pairing: function() {
          return pairing;
        }
      }
    });
    modalInstance.result.then(function (pairing) {
      TournamentRunner.reportResult(vm.uuid, pairing)
         .success(function(data) {
            vm.tourn = data;
         });
    });
    
  };
}])

// Please note that $uibModalInstance represents a modal window (instance) dependency.
// It is not the same as the $uibModal service used above.

.controller('ModalInstanceCtrl', function ($uibModalInstance, pairing) {

  var mic = this;

  mic.pairing = pairing;
  
  mic.pick = function (p1Win, p2Win, draw) {
    mic.pairing.result = {
      p1Win: p1Win,
      p2Win: p2Win,
      draw: draw 
    };
    $uibModalInstance.close(pairing);
  };

  mic.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
});