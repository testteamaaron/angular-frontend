'use strict';

angular.module('swiss.add-players', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/:uuid/add-players', {
    templateUrl: 'pages/add-players/add-players.html',
    controller: 'setupCtrl',
    controllerAs: 'vm'    
  });
}])

.controller('setupCtrl',
            ["TournamentManager", "TournamentRunner",
             "$routeParams", "$location",
              function(TournamentManager, TournamentRunner,
                       $routeParams, $location) {
    var vm = this;

    vm.addPlayer = function(player) {
        vm.tourn.players.push(angular.copy(player));
        TournamentRunner.addPlayer(vm.uuid, player)
        .success(function(data) {
            vm.tourn = data;
        });
        vm.player.name = "";
    } 

    vm.submitPlayers = function() {
        TournamentRunner.finalisePlayers(vm.uuid).success(function() {
            $location.path(vm.uuid + "/tourn")
        });
    }

    vm.uuid = $routeParams.uuid; 
    TournamentManager.fetchTourn(vm.uuid) 
         .success(function(data) {
            vm.tourn = data;
       }).error(function(data) {
            $location.path("/");
    });
    


}]);
