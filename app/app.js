'use strict';

// Declare app level module which depends on views, and components
angular.module('swiss', [
  'ngRoute',
  'swiss.start',
  'swiss.add-players',
  'swiss.tourn',
  'swiss.service',
  'swiss.version'
]).
config(['$routeProvider', function($routeProvider) {
  $routeProvider.otherwise({redirectTo: '/start'});
}]);
