angular.module('swiss.service', [])
.factory('TournamentManager', ['$http', function($http) {
  return {
    newTourn: function(preTourn) {
      return $http.post('http://localhost:8080/');
    },

    fetchTourn: function(uuid) {
      return $http.get('http://localhost:8080/'+uuid);
    }
  }    
}])
.factory('TournamentRunner', [ '$http', function($http) {
  return {
    addPlayer: function(uuid, player) {
      return $http.post('http://localhost:8080/'
                        +uuid + '/player?person=' + player.name)
    },
    finalisePlayers: function(uuid) {
      return $http.post('http://localhost:8080/' + uuid);
    },
    reportResult: function(uuid, pairing) {
      return $http.post("http://localhost:8080/" + uuid + "/pairings/" + pairing.pairId, pairing.result);
    },
    completeRound: function(uuid) {
     return $http.post("http://localhost:8080/" + uuid + "/pairings/"); 
    }
  }
}]
);