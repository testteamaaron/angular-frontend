'use strict';

angular.module('swiss.add-players', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/:uuid/add-players', {
    templateUrl: 'pages/add-players/add-players.html',
    controller: 'setupCtrl',
    controllerAs: 'vm'    
  });
}])

.controller('setupCtrl',
            ["TournamentManager", "TournamentRunner",
             "$routeParams", "$location",
              function(TournamentManager, TournamentRunner,
                       $routeParams, $location) {
    var vm = this;

    vm.addPlayer = function(player) {
        vm.tourn.players.push(angular.copy(player));
        TournamentRunner.addPlayer(vm.uuid, player)
        .success(function(data) {
            vm.tourn = data;
        });
        vm.player.name = "";
    } 

    vm.submitPlayers = function() {
        TournamentRunner.finalisePlayers(vm.uuid).success(function() {
            $location.path(vm.uuid + "/tourn")
        });
    }

    vm.uuid = $routeParams.uuid; 
    TournamentManager.fetchTourn(vm.uuid) 
         .success(function(data) {
            vm.tourn = data;
       }).error(function(data) {
            $location.path("/");
    });
    


}]);

'use strict';

angular.module('swiss.start', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/start', {
    templateUrl: 'pages/start/start.html',
    controller: 'startCtrl',
    controllerAs: 'vm'
  });
}])

.controller('startCtrl',
            ["TournamentManager",
             "$location", function(TournamentManager, $location) {
    var vm = this;   
    vm.resolveTournament = function(preTourn) {
        
    }    

    vm.newTournament = function(preTourn) {
        TournamentManager.newTourn(preTourn).success(function(data) {
            vm.tourn = data;
            $location.path(vm.tourn.uuid + "/add-players/");
        });
    }
   
}]);

'use strict';

angular.module('swiss.tourn', ['ngRoute', 'ngAnimate', 'ui.bootstrap'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/:uuid/tourn', {
    templateUrl: 'pages/tourn/tourn.html',
    controller: 'tournCtrl',
    controllerAs: 'vm'
  });
}])

.controller('tournCtrl',
            ["TournamentManager", "TournamentRunner", "$location", "$routeParams",  "$uibModal",
             function(TournamentManager, TournamentRunner, $location, $routeParams, $uibModal) {
    var vm = this;   
    
    vm.uuid = $routeParams.uuid; 
    TournamentManager.fetchTourn(vm.uuid) 
         .success(function(data) {
            vm.tourn = data;
       }).error(function(data) {
            $location.path("/");
    });

  vm.finalise = function() {
    TournamentRunner.completeRound(vm.uuid)
      .success(function(data) {
        vm.tourn = data;
      });
  };

  vm.open = function (pairing) {
    var modalInstance = $uibModal.open({
      animation: true,
      templateUrl: 'myModalContent.html',
      controller: 'ModalInstanceCtrl',
      controllerAs: 'mic',
      resolve: {
        pairing: function() {
          return pairing;
        }
      }
    });
    modalInstance.result.then(function (pairing) {
      TournamentRunner.reportResult(vm.uuid, pairing)
         .success(function(data) {
            vm.tourn = data;
         });
    });
    
  };
}])

// Please note that $uibModalInstance represents a modal window (instance) dependency.
// It is not the same as the $uibModal service used above.

.controller('ModalInstanceCtrl', function ($uibModalInstance, pairing) {

  var mic = this;

  mic.pairing = pairing;
  
  mic.pick = function (p1Win, p2Win, draw) {
    mic.pairing.result = {
      p1Win: p1Win,
      p2Win: p2Win,
      draw: draw 
    };
    $uibModalInstance.close(pairing);
  };

  mic.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
});
angular.module('swiss.service', [])
.factory('TournamentManager', ['$http', function($http) {
  return {
    newTourn: function(preTourn) {
      return $http.post('http://localhost:8080/');
    },

    fetchTourn: function(uuid) {
      return $http.get('http://localhost:8080/'+uuid);
    }
  }    
}])
.factory('TournamentRunner', [ '$http', function($http) {
  return {
    addPlayer: function(uuid, player) {
      return $http.post('http://localhost:8080/'
                        +uuid + '/player?person=' + player.name)
    },
    finalisePlayers: function(uuid) {
      return $http.post('http://localhost:8080/' + uuid);
    },
    reportResult: function(uuid, pairing) {
      return $http.post("http://localhost:8080/" + uuid + "/pairings/" + pairing.pairId, pairing.result);
    },
    completeRound: function(uuid) {
     return $http.post("http://localhost:8080/" + uuid + "/pairings/"); 
    }
  }
}]
);
'use strict';

angular.module('swiss.version.interpolate-filter', [])

.filter('interpolate', ['version', function(version) {
  return function(text) {
    return String(text).replace(/\%VERSION\%/mg, version);
  };
}]);

'use strict';

angular.module('swiss.version.version-directive', [])

.directive('appVersion', ['version', function(version) {
  return function(scope, elm, attrs) {
    elm.text(version);
  };
}]);

'use strict';

angular.module('swiss.version', [
  'swiss.version.interpolate-filter',
  'swiss.version.version-directive'
])

.value('version', '0.1');

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFkZC1wbGF5ZXJzL2FkZC1wbGF5ZXJzLmpzIiwic3RhcnQvc3RhcnQuanMiLCJ0b3Vybi90b3Vybi5qcyIsInRvdXJuYW1lbnQtbWFuYWdlci5qcyIsInZlcnNpb24vaW50ZXJwb2xhdGUtZmlsdGVyLmpzIiwidmVyc2lvbi92ZXJzaW9uLWRpcmVjdGl2ZS5qcyIsInZlcnNpb24vdmVyc2lvbi5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQzdDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDNUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDM0VBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQzdCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ1RBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDVEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwiZmlsZSI6ImFsbC5qcyIsInNvdXJjZXNDb250ZW50IjpbIid1c2Ugc3RyaWN0JztcblxuYW5ndWxhci5tb2R1bGUoJ3N3aXNzLmFkZC1wbGF5ZXJzJywgWyduZ1JvdXRlJ10pXG5cbi5jb25maWcoWyckcm91dGVQcm92aWRlcicsIGZ1bmN0aW9uKCRyb3V0ZVByb3ZpZGVyKSB7XG4gICRyb3V0ZVByb3ZpZGVyLndoZW4oJy86dXVpZC9hZGQtcGxheWVycycsIHtcbiAgICB0ZW1wbGF0ZVVybDogJ3BhZ2VzL2FkZC1wbGF5ZXJzL2FkZC1wbGF5ZXJzLmh0bWwnLFxuICAgIGNvbnRyb2xsZXI6ICdzZXR1cEN0cmwnLFxuICAgIGNvbnRyb2xsZXJBczogJ3ZtJyAgICBcbiAgfSk7XG59XSlcblxuLmNvbnRyb2xsZXIoJ3NldHVwQ3RybCcsXG4gICAgICAgICAgICBbXCJUb3VybmFtZW50TWFuYWdlclwiLCBcIlRvdXJuYW1lbnRSdW5uZXJcIixcbiAgICAgICAgICAgICBcIiRyb3V0ZVBhcmFtc1wiLCBcIiRsb2NhdGlvblwiLFxuICAgICAgICAgICAgICBmdW5jdGlvbihUb3VybmFtZW50TWFuYWdlciwgVG91cm5hbWVudFJ1bm5lcixcbiAgICAgICAgICAgICAgICAgICAgICAgJHJvdXRlUGFyYW1zLCAkbG9jYXRpb24pIHtcbiAgICB2YXIgdm0gPSB0aGlzO1xuXG4gICAgdm0uYWRkUGxheWVyID0gZnVuY3Rpb24ocGxheWVyKSB7XG4gICAgICAgIHZtLnRvdXJuLnBsYXllcnMucHVzaChhbmd1bGFyLmNvcHkocGxheWVyKSk7XG4gICAgICAgIFRvdXJuYW1lbnRSdW5uZXIuYWRkUGxheWVyKHZtLnV1aWQsIHBsYXllcilcbiAgICAgICAgLnN1Y2Nlc3MoZnVuY3Rpb24oZGF0YSkge1xuICAgICAgICAgICAgdm0udG91cm4gPSBkYXRhO1xuICAgICAgICB9KTtcbiAgICAgICAgdm0ucGxheWVyLm5hbWUgPSBcIlwiO1xuICAgIH0gXG5cbiAgICB2bS5zdWJtaXRQbGF5ZXJzID0gZnVuY3Rpb24oKSB7XG4gICAgICAgIFRvdXJuYW1lbnRSdW5uZXIuZmluYWxpc2VQbGF5ZXJzKHZtLnV1aWQpLnN1Y2Nlc3MoZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAkbG9jYXRpb24ucGF0aCh2bS51dWlkICsgXCIvdG91cm5cIilcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgdm0udXVpZCA9ICRyb3V0ZVBhcmFtcy51dWlkOyBcbiAgICBUb3VybmFtZW50TWFuYWdlci5mZXRjaFRvdXJuKHZtLnV1aWQpIFxuICAgICAgICAgLnN1Y2Nlc3MoZnVuY3Rpb24oZGF0YSkge1xuICAgICAgICAgICAgdm0udG91cm4gPSBkYXRhO1xuICAgICAgIH0pLmVycm9yKGZ1bmN0aW9uKGRhdGEpIHtcbiAgICAgICAgICAgICRsb2NhdGlvbi5wYXRoKFwiL1wiKTtcbiAgICB9KTtcbiAgICBcblxuXG59XSk7XG4iLCIndXNlIHN0cmljdCc7XG5cbmFuZ3VsYXIubW9kdWxlKCdzd2lzcy5zdGFydCcsIFsnbmdSb3V0ZSddKVxuXG4uY29uZmlnKFsnJHJvdXRlUHJvdmlkZXInLCBmdW5jdGlvbigkcm91dGVQcm92aWRlcikge1xuICAkcm91dGVQcm92aWRlci53aGVuKCcvc3RhcnQnLCB7XG4gICAgdGVtcGxhdGVVcmw6ICdwYWdlcy9zdGFydC9zdGFydC5odG1sJyxcbiAgICBjb250cm9sbGVyOiAnc3RhcnRDdHJsJyxcbiAgICBjb250cm9sbGVyQXM6ICd2bSdcbiAgfSk7XG59XSlcblxuLmNvbnRyb2xsZXIoJ3N0YXJ0Q3RybCcsXG4gICAgICAgICAgICBbXCJUb3VybmFtZW50TWFuYWdlclwiLFxuICAgICAgICAgICAgIFwiJGxvY2F0aW9uXCIsIGZ1bmN0aW9uKFRvdXJuYW1lbnRNYW5hZ2VyLCAkbG9jYXRpb24pIHtcbiAgICB2YXIgdm0gPSB0aGlzOyAgIFxuICAgIHZtLnJlc29sdmVUb3VybmFtZW50ID0gZnVuY3Rpb24ocHJlVG91cm4pIHtcbiAgICAgICAgXG4gICAgfSAgICBcblxuICAgIHZtLm5ld1RvdXJuYW1lbnQgPSBmdW5jdGlvbihwcmVUb3Vybikge1xuICAgICAgICBUb3VybmFtZW50TWFuYWdlci5uZXdUb3VybihwcmVUb3Vybikuc3VjY2VzcyhmdW5jdGlvbihkYXRhKSB7XG4gICAgICAgICAgICB2bS50b3VybiA9IGRhdGE7XG4gICAgICAgICAgICAkbG9jYXRpb24ucGF0aCh2bS50b3Vybi51dWlkICsgXCIvYWRkLXBsYXllcnMvXCIpO1xuICAgICAgICB9KTtcbiAgICB9XG4gICBcbn1dKTtcbiIsIid1c2Ugc3RyaWN0JztcblxuYW5ndWxhci5tb2R1bGUoJ3N3aXNzLnRvdXJuJywgWyduZ1JvdXRlJywgJ25nQW5pbWF0ZScsICd1aS5ib290c3RyYXAnXSlcblxuLmNvbmZpZyhbJyRyb3V0ZVByb3ZpZGVyJywgZnVuY3Rpb24oJHJvdXRlUHJvdmlkZXIpIHtcbiAgJHJvdXRlUHJvdmlkZXIud2hlbignLzp1dWlkL3RvdXJuJywge1xuICAgIHRlbXBsYXRlVXJsOiAncGFnZXMvdG91cm4vdG91cm4uaHRtbCcsXG4gICAgY29udHJvbGxlcjogJ3RvdXJuQ3RybCcsXG4gICAgY29udHJvbGxlckFzOiAndm0nXG4gIH0pO1xufV0pXG5cbi5jb250cm9sbGVyKCd0b3VybkN0cmwnLFxuICAgICAgICAgICAgW1wiVG91cm5hbWVudE1hbmFnZXJcIiwgXCJUb3VybmFtZW50UnVubmVyXCIsIFwiJGxvY2F0aW9uXCIsIFwiJHJvdXRlUGFyYW1zXCIsICBcIiR1aWJNb2RhbFwiLFxuICAgICAgICAgICAgIGZ1bmN0aW9uKFRvdXJuYW1lbnRNYW5hZ2VyLCBUb3VybmFtZW50UnVubmVyLCAkbG9jYXRpb24sICRyb3V0ZVBhcmFtcywgJHVpYk1vZGFsKSB7XG4gICAgdmFyIHZtID0gdGhpczsgICBcbiAgICBcbiAgICB2bS51dWlkID0gJHJvdXRlUGFyYW1zLnV1aWQ7IFxuICAgIFRvdXJuYW1lbnRNYW5hZ2VyLmZldGNoVG91cm4odm0udXVpZCkgXG4gICAgICAgICAuc3VjY2VzcyhmdW5jdGlvbihkYXRhKSB7XG4gICAgICAgICAgICB2bS50b3VybiA9IGRhdGE7XG4gICAgICAgfSkuZXJyb3IoZnVuY3Rpb24oZGF0YSkge1xuICAgICAgICAgICAgJGxvY2F0aW9uLnBhdGgoXCIvXCIpO1xuICAgIH0pO1xuXG4gIHZtLmZpbmFsaXNlID0gZnVuY3Rpb24oKSB7XG4gICAgVG91cm5hbWVudFJ1bm5lci5jb21wbGV0ZVJvdW5kKHZtLnV1aWQpXG4gICAgICAuc3VjY2VzcyhmdW5jdGlvbihkYXRhKSB7XG4gICAgICAgIHZtLnRvdXJuID0gZGF0YTtcbiAgICAgIH0pO1xuICB9O1xuXG4gIHZtLm9wZW4gPSBmdW5jdGlvbiAocGFpcmluZykge1xuICAgIHZhciBtb2RhbEluc3RhbmNlID0gJHVpYk1vZGFsLm9wZW4oe1xuICAgICAgYW5pbWF0aW9uOiB0cnVlLFxuICAgICAgdGVtcGxhdGVVcmw6ICdteU1vZGFsQ29udGVudC5odG1sJyxcbiAgICAgIGNvbnRyb2xsZXI6ICdNb2RhbEluc3RhbmNlQ3RybCcsXG4gICAgICBjb250cm9sbGVyQXM6ICdtaWMnLFxuICAgICAgcmVzb2x2ZToge1xuICAgICAgICBwYWlyaW5nOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICByZXR1cm4gcGFpcmluZztcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0pO1xuICAgIG1vZGFsSW5zdGFuY2UucmVzdWx0LnRoZW4oZnVuY3Rpb24gKHBhaXJpbmcpIHtcbiAgICAgIFRvdXJuYW1lbnRSdW5uZXIucmVwb3J0UmVzdWx0KHZtLnV1aWQsIHBhaXJpbmcpXG4gICAgICAgICAuc3VjY2VzcyhmdW5jdGlvbihkYXRhKSB7XG4gICAgICAgICAgICB2bS50b3VybiA9IGRhdGE7XG4gICAgICAgICB9KTtcbiAgICB9KTtcbiAgICBcbiAgfTtcbn1dKVxuXG4vLyBQbGVhc2Ugbm90ZSB0aGF0ICR1aWJNb2RhbEluc3RhbmNlIHJlcHJlc2VudHMgYSBtb2RhbCB3aW5kb3cgKGluc3RhbmNlKSBkZXBlbmRlbmN5LlxuLy8gSXQgaXMgbm90IHRoZSBzYW1lIGFzIHRoZSAkdWliTW9kYWwgc2VydmljZSB1c2VkIGFib3ZlLlxuXG4uY29udHJvbGxlcignTW9kYWxJbnN0YW5jZUN0cmwnLCBmdW5jdGlvbiAoJHVpYk1vZGFsSW5zdGFuY2UsIHBhaXJpbmcpIHtcblxuICB2YXIgbWljID0gdGhpcztcblxuICBtaWMucGFpcmluZyA9IHBhaXJpbmc7XG4gIFxuICBtaWMucGljayA9IGZ1bmN0aW9uIChwMVdpbiwgcDJXaW4sIGRyYXcpIHtcbiAgICBtaWMucGFpcmluZy5yZXN1bHQgPSB7XG4gICAgICBwMVdpbjogcDFXaW4sXG4gICAgICBwMldpbjogcDJXaW4sXG4gICAgICBkcmF3OiBkcmF3IFxuICAgIH07XG4gICAgJHVpYk1vZGFsSW5zdGFuY2UuY2xvc2UocGFpcmluZyk7XG4gIH07XG5cbiAgbWljLmNhbmNlbCA9IGZ1bmN0aW9uICgpIHtcbiAgICAkdWliTW9kYWxJbnN0YW5jZS5kaXNtaXNzKCdjYW5jZWwnKTtcbiAgfTtcbn0pOyIsImFuZ3VsYXIubW9kdWxlKCdzd2lzcy5zZXJ2aWNlJywgW10pXG4uZmFjdG9yeSgnVG91cm5hbWVudE1hbmFnZXInLCBbJyRodHRwJywgZnVuY3Rpb24oJGh0dHApIHtcbiAgcmV0dXJuIHtcbiAgICBuZXdUb3VybjogZnVuY3Rpb24ocHJlVG91cm4pIHtcbiAgICAgIHJldHVybiAkaHR0cC5wb3N0KCdodHRwOi8vbG9jYWxob3N0OjgwODAvJyk7XG4gICAgfSxcblxuICAgIGZldGNoVG91cm46IGZ1bmN0aW9uKHV1aWQpIHtcbiAgICAgIHJldHVybiAkaHR0cC5nZXQoJ2h0dHA6Ly9sb2NhbGhvc3Q6ODA4MC8nK3V1aWQpO1xuICAgIH1cbiAgfSAgICBcbn1dKVxuLmZhY3RvcnkoJ1RvdXJuYW1lbnRSdW5uZXInLCBbICckaHR0cCcsIGZ1bmN0aW9uKCRodHRwKSB7XG4gIHJldHVybiB7XG4gICAgYWRkUGxheWVyOiBmdW5jdGlvbih1dWlkLCBwbGF5ZXIpIHtcbiAgICAgIHJldHVybiAkaHR0cC5wb3N0KCdodHRwOi8vbG9jYWxob3N0OjgwODAvJ1xuICAgICAgICAgICAgICAgICAgICAgICAgK3V1aWQgKyAnL3BsYXllcj9wZXJzb249JyArIHBsYXllci5uYW1lKVxuICAgIH0sXG4gICAgZmluYWxpc2VQbGF5ZXJzOiBmdW5jdGlvbih1dWlkKSB7XG4gICAgICByZXR1cm4gJGh0dHAucG9zdCgnaHR0cDovL2xvY2FsaG9zdDo4MDgwLycgKyB1dWlkKTtcbiAgICB9LFxuICAgIHJlcG9ydFJlc3VsdDogZnVuY3Rpb24odXVpZCwgcGFpcmluZykge1xuICAgICAgcmV0dXJuICRodHRwLnBvc3QoXCJodHRwOi8vbG9jYWxob3N0OjgwODAvXCIgKyB1dWlkICsgXCIvcGFpcmluZ3MvXCIgKyBwYWlyaW5nLnBhaXJJZCwgcGFpcmluZy5yZXN1bHQpO1xuICAgIH0sXG4gICAgY29tcGxldGVSb3VuZDogZnVuY3Rpb24odXVpZCkge1xuICAgICByZXR1cm4gJGh0dHAucG9zdChcImh0dHA6Ly9sb2NhbGhvc3Q6ODA4MC9cIiArIHV1aWQgKyBcIi9wYWlyaW5ncy9cIik7IFxuICAgIH1cbiAgfVxufV1cbik7IiwiJ3VzZSBzdHJpY3QnO1xuXG5hbmd1bGFyLm1vZHVsZSgnc3dpc3MudmVyc2lvbi5pbnRlcnBvbGF0ZS1maWx0ZXInLCBbXSlcblxuLmZpbHRlcignaW50ZXJwb2xhdGUnLCBbJ3ZlcnNpb24nLCBmdW5jdGlvbih2ZXJzaW9uKSB7XG4gIHJldHVybiBmdW5jdGlvbih0ZXh0KSB7XG4gICAgcmV0dXJuIFN0cmluZyh0ZXh0KS5yZXBsYWNlKC9cXCVWRVJTSU9OXFwlL21nLCB2ZXJzaW9uKTtcbiAgfTtcbn1dKTtcbiIsIid1c2Ugc3RyaWN0JztcblxuYW5ndWxhci5tb2R1bGUoJ3N3aXNzLnZlcnNpb24udmVyc2lvbi1kaXJlY3RpdmUnLCBbXSlcblxuLmRpcmVjdGl2ZSgnYXBwVmVyc2lvbicsIFsndmVyc2lvbicsIGZ1bmN0aW9uKHZlcnNpb24pIHtcbiAgcmV0dXJuIGZ1bmN0aW9uKHNjb3BlLCBlbG0sIGF0dHJzKSB7XG4gICAgZWxtLnRleHQodmVyc2lvbik7XG4gIH07XG59XSk7XG4iLCIndXNlIHN0cmljdCc7XG5cbmFuZ3VsYXIubW9kdWxlKCdzd2lzcy52ZXJzaW9uJywgW1xuICAnc3dpc3MudmVyc2lvbi5pbnRlcnBvbGF0ZS1maWx0ZXInLFxuICAnc3dpc3MudmVyc2lvbi52ZXJzaW9uLWRpcmVjdGl2ZSdcbl0pXG5cbi52YWx1ZSgndmVyc2lvbicsICcwLjEnKTtcbiJdLCJzb3VyY2VSb290IjoiL3NvdXJjZS8ifQ==
