'use strict';

angular.module('swiss.version', [
  'swiss.version.interpolate-filter',
  'swiss.version.version-directive'
])

.value('version', '0.1');
